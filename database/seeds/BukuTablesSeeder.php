<?php

use Illuminate\Database\Seeder;

class BukuTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\buku::create([
        'judul'  => str_random(20),
        'Pengarang' => str_random(10)
]);
    }
}
